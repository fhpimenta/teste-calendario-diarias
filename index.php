<?php

require 'vendor/autoload.php';

// Data Inicial do Plano
$dataInicial = \Carbon\Carbon::parse('2018-09-10')->toDateString();
// Data final do Plano,caso seja um plano Semestral
$dataFinal = \Carbon\Carbon::parse('2018-09-10')->addMonths(6)->toDateString();

// Lista de Feriados, no formato d-m-Y
$feriados = [
    '2018-09-07', // Dia da Indepedência
    '2018-09-08', // Aniversario de São Luís
    '2018-10-12', // Dia de Nossa Senhora Aparecida
    '2018-11-02', // Dia de Finados
    '2018-11-15', // Dia da Proclamação da República
    '2018-12-08', // Dia de uma santa de São Luís
    '2018-12-25', // Natal
    '2018-12-31', // Reveillon
    '2019-01-01', // 1º de Janeiro
    '2019-03-05', // Carnaval
    '2019-03-06' // Cinzas
];

/* Dias da semana que o cliente escolheu

Segue a seguinte orientação:
0 -> Domingo
1 -> Segunda-feira
2 -> Terça-feira
3 -> Quarta-feira
4 -> Quinta-feira
5 -> Sexta-feira
6 -> Sabado

*/
$diasSemana = [1,3];

// Criação do período que engloba o plano
$period = \Carbon\CarbonPeriod::create($dataInicial, $dataFinal);

// Filtro para selecionar somente os dias da semana que o cliente escolheu
$daysOfWeekFilter = function ($date) use ($diasSemana) {
    return in_array($date->dayOfWeek, [1,3]);
};
// Aplicação do filtro
$period->filter($daysOfWeekFilter);

// Recalculo da data final do plano
$dataFinal = $period->getEndDate()->toDateString();
$day = 0;
$interval = $period->toArray();
foreach ($interval as $date) {
    // Caso alguma data caia em algum feriado, jogar a data final para o proximo
    if (in_array($date->toDateString(), $feriados)) {
        $obj = \Carbon\Carbon::parse($dataFinal)->next($diasSemana[$day]);
        $interval[] = $obj;
        $dataFinal = $obj->toDateString();
        $day += 1;
    }

    if ($day == count($diasSemana)) {
        $day = 0;
    }
}

// Filtro de Feriados
$feriadoFilter = function ($date) use($feriados, $period) {
    return !in_array($date->toDateString(), $feriados);
};

// Seta a nova data final
$period->setEndDate(\Carbon\Carbon::parse($dataFinal)->addDay()->toDateString(), true);
$period->addFilter($feriadoFilter);

// Exibir na tela o calendário de datas
$days = [];
foreach ($period as $date) {
    $days[] = $date->format('d/m/Y');
}
echo implode(', ', $days);

echo "\nNumero de semanas: ".count($period->toArray())/count($diasSemana);